Pod::Spec.new do |spec|
  spec.name             = 'SearchableTableView'
  spec.version          = '1.1'
  spec.license          = { :type => 'MIT' }
  spec.homepage         = 'https://bitbucket.org/nova-project/searchabletableview'
  spec.authors          = { 'Bazyli Zygan' => 'bazyl@novaproject.net' }
  spec.summary          = 'A table view that will help you with your search mechanisms in the application.'
  spec.source           = { :git => 'https://bitbucket.org/nova-project/searchabletableview.git', :tag => 'v1.1' }
  spec.source_files     = 'SearchableTableView/*'
  spec.ios.deployment_target = '8.0'
  spec.requires_arc = true  
end
