//
//  ViewController.swift
//  SearchableTableView Example
//
//  Created by Bazyli Zygan on 01.01.2016.
//  Copyright © 2016 Nova Project. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    let items = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fithteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty"]
    
    var currentItems: [String] = []
    var lastQuery: String?
    
    @IBOutlet weak var tableView: SearchableTableView!
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: "ViewController", bundle: nil)
        title = "Searchable TableView"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        updateValues()
        return currentItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        updateValues()
        var cell = tableView.dequeueReusableCellWithIdentifier("TestCell")
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "TestCell")
        }
        
        cell?.textLabel?.text = currentItems[indexPath.row]
        
        return cell!
    }
    
    private func updateValues() {
        if tableView.searchQuery != nil && tableView.searchQuery!.characters.count > 0 {
            if lastQuery == nil || lastQuery != tableView.searchQuery {
                lastQuery = tableView.searchQuery
                currentItems = items.filter({ (item) -> Bool in
                    return item.lowercaseString.containsString(tableView.searchQuery!.lowercaseString)
                })
            }
        } else {
            lastQuery = nil
            currentItems = items
        }
    }
}

